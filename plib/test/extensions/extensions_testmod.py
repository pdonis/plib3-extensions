#!/usr/bin/env python3
"""
TEST.EXTENSIONS.EXTENSIONS_TESTMOD.PY -- utility module for Python/C extension tests
Copyright (C) 2008-2022 by Peter A. Donis

Released under the GNU General Public License, Version 2
See the LICENSE and README files for more information

This module provides a Python interface to the C code
that is needed to test PLIB3.EXTENSIONS's Python/C extensions.
"""

from ._extensions_testmod import *
