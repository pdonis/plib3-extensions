#!/usr/bin/env python3
"""
Sub-Package TEST.EXTENSIONS of Package PLIB3
Copyright (C) 2008-2022 by Peter A. Donis

Released under the GNU General Public License, Version 2
See the LICENSE and README files for more information

This sub-package contains the PLIB3.EXTENSIONS test suite.
"""
